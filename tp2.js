// AUXILIARES ------------------------------------------------------------------
expresion = {
	esFormaNormal : true,
	esSingleton : false,
	evaluar : function () {
		termino = this;
		while(!termino.esFormaNormal){
			termino = termino.reducir();
		}
		return termino;
	}
}

tipo = {
	esSingleton : false
}

// EJERCICIO 1 -----------------------------------------------------------------
TT = Object.create(expresion);
TT.esSingleton = true;
FF = Object.create(expresion);
FF.esSingleton = true;
Bool = Object.create(tipo);
Bool.esSingleton = true;

tipoDeFuncion = function(tipoDominio,tipoImagen){
	this.tipoDominio = tipoDominio;
	this.tipoImagen = tipoImagen;
	Object.setPrototypeOf(Object.getPrototypeOf(this),tipo);
};

abstraccion = function(variable,tipoDeVariable,codigo){
	this.variable = variable;
	this.tipoDeVariable = tipoDeVariable;
	this.codigo = codigo;
	Object.setPrototypeOf(Object.getPrototypeOf(this),expresion);
};

variable = function(nombre) {
	this.nombre = nombre;
	Object.setPrototypeOf(Object.getPrototypeOf(this),expresion);
};

aplicacion = function(izquierdo,derecho) {
	this.izquierdo = izquierdo;
	this.derecho = derecho;
	this.esFormaNormal = false;
	Object.setPrototypeOf(Object.getPrototypeOf(this),expresion);
};

// TESTS -----------------------------------------------------------------------

variableX = new variable("x");
tipoDeFuncionBoolEnBool = new tipoDeFuncion(Bool,Bool);
identidadDeBooleanos = new abstraccion(variableX, Bool, variableX);
trueAplicadoALaIdentidad = new aplicacion(identidadDeBooleanos,TT);

// Tests de igualdad para los singletons de tipo y valores constantes
if(TT != TT){
	console.log("Falló el test de igualdad del término TT.");
};
if(FF != FF){
	console.log("Falló el test de igualdad del término FF.");
};
if(TT == FF){
	console.log("Falló el test de distinción de los término TT y FF.");
};
if(Bool != Bool){
	console.log("Falló el test de igualdad del tipo Bool.");
};
if(Bool == TT){
	console.log("Falló el test de distinción de Bool y TT.");
};
if(Bool == FF){
	console.log("Falló el test de distinción de Bool y FF.");
};

// Tests de prototipos para TT, FF y Bool
if(Object.getPrototypeOf(TT) != expresion){
	console.log("Falló el test de prototipo de TT.");
};
if(Object.getPrototypeOf(FF) != expresion){
	console.log("Falló el test de prototipo de FF.");
};
if(Object.getPrototypeOf(Bool) != tipo){
	console.log("Falló el test de prototipo de Bool.");
};

// Tests unitarios de tipoDeFuncion
if(tipoDeFuncionBoolEnBool.tipoDominio != Bool){
	console.log("Falló el test de tipoDominio simple en tipoDeFuncion.");
};
if(tipoDeFuncionBoolEnBool.tipoImagen != Bool){
	console.log("Falló el test de tipoImagen simple en tipoDeFuncion.");
};
if(Object.getPrototypeOf(Object.getPrototypeOf(tipoDeFuncionBoolEnBool)) != tipo){
	console.log("Falló el test de prototipo de tipoDeFuncion.");
};

// Tests unitarios de variable
if(variableX.nombre != "x"){
	console.log("Falló el test de nombre de variable.");
};
if(Object.getPrototypeOf(Object.getPrototypeOf(variableX)) != expresion){
	console.log("Falló el test de prototipo de variable.");
};

// Tests unitarios de abstracción
if(identidadDeBooleanos.variable.nombre != "x"){
	console.log("Falló el test de nombre de variable para abstraccion.");
}
if(identidadDeBooleanos.tipoDeVariable != Bool){
	console.log("Falló el test de tipo de variable para abstraccion.");
}
if(Object.getPrototypeOf(Object.getPrototypeOf(identidadDeBooleanos)) != expresion){
	console.log("Falló el test de prototipo de abstraccion.");
};

// Tests unitarios de aplicación
if(Object.getPrototypeOf(Object.getPrototypeOf(trueAplicadoALaIdentidad)) != expresion){
	console.log("Falló el test de prototipo de aplicacion.");
};

// EJERCICIO 2 -----------------------------------------------------------------
TT.toString = function(){
	return "true";
};

FF.toString = function(){
	return "false";
};

Bool.toString = function(){
	return "Bool";
};

tipoDeFuncion.prototype.toString = function(){
	return this.tipoDominio.toString() + "->" + this.tipoImagen.toString();
};

variable.prototype.toString = function(){
	return this.nombre.toString();
};

abstraccion.prototype.toString = function(){
	return "\\"+this.variable.toString()+":"+this.tipoDeVariable.toString()+"."+this.codigo.toString();
};

aplicacion.prototype.toString = function(){
	return "(" + this.izquierdo.toString() + " " + this.derecho.toString() + ")";
};

// TESTS -----------------------------------------------------------------------

// Tests de impresiones por pantalla mediante toString()
if(TT.toString() != "true"){
	console.log("Falló el test de impresión por pantalla del TT.");
}

if(FF.toString() != "false"){
	console.log("Falló el test de impresión por pantalla del FF.");
}

if(Bool.toString() != "Bool"){
	console.log("Falló el test de impresión por pantalla del Bool.");
}

if(tipoDeFuncionBoolEnBool != "Bool->Bool"){
	console.log("Falló el test de impresión por pantalla del tipoDeFuncion Bool->Bool.");
}

if(new tipoDeFuncion(tipoDeFuncionBoolEnBool,Bool) != "Bool->Bool->Bool"){
	console.log("Falló el test de impresión por pantalla del tipoDeFuncion Bool->Bool->Bool.");
}

if(variableX.toString() != "x"){
	console.log("Falló el test de impresión por pantalla de la variable.");
}

if(identidadDeBooleanos.toString() != "\\x:Bool.x"){
	console.log("Falló el test de impresión por pantalla de la abstracción.");
}

if(trueAplicadoALaIdentidad.toString() != "(\\x:Bool.x true)"){
	console.log("Falló el test de impresión por pantalla de la aplicación.");
}

// EJERCICIO 3 -----------------------------------------------------------------
TT.deepCopy = function(){
	return this;
};

FF.deepCopy = function(){
	return this;
};

Bool.deepCopy = function(){
	return this;
};

tipoDeFuncion.prototype.deepCopy = function(){
	return new tipoDeFuncion(this.tipoDominio.deepCopy(),this.tipoImagen.deepCopy());
};

abstraccion.prototype.deepCopy = function(){
	return new abstraccion(this.variable.deepCopy(),this.tipoDeVariable.deepCopy(),this.codigo.deepCopy());
};

variable.prototype.deepCopy = function(){
	return new variable(this.nombre);
};

aplicacion.prototype.deepCopy = function(){
	return new aplicacion(this.izquierdo.deepCopy(),this.derecho.deepCopy());
};

// TESTS -----------------------------------------------------------------------

// El deepCopy() para singletons (TT, FF, Bool) debe dar el mismo objeto
if(TT.deepCopy() != TT || !TT.esSingleton){
	console.log("Falló el test de deepCopy() para TT.");
}

if(FF.deepCopy() != FF || !FF.esSingleton){
	console.log("Falló el test de deepCopy() para FF.");
}

if(Bool.deepCopy() != Bool || !Bool.esSingleton){
	console.log("Falló el test de deepCopy() para Bool.");
}

if(
	tipoDeFuncionBoolEnBool.deepCopy() == tipoDeFuncionBoolEnBool ||
	tipoDeFuncionBoolEnBool.deepCopy().toString() != tipoDeFuncionBoolEnBool.toString()){
	console.log("Falló el test de deepCopy() para tipoDeFuncion Bool->Bool.");
}

if(
	identidadDeBooleanos.deepCopy() == identidadDeBooleanos ||
	identidadDeBooleanos.deepCopy().toString() != identidadDeBooleanos.toString()){
	console.log("Falló el test de deepCopy() para aplicación.");
}

if(
	variableX.deepCopy() == variableX ||
	variableX.deepCopy().toString() != variableX.toString()){
	console.log("Falló el test de deepCopy() para variable.");
}

if(
	trueAplicadoALaIdentidad.deepCopy() == trueAplicadoALaIdentidad ||
	trueAplicadoALaIdentidad.deepCopy().toString() != trueAplicadoALaIdentidad.toString()){
	console.log("Falló el test de deepCopy() para abstracción.");
}

// EJERCICIO 4 -----------------------------------------------------------------
TT.sust = function(nombreVariable,termino){
	return this;
};

FF.sust = function(nombreVariable,termino){
	return this;
};

Bool.sust = function(nombreVariable,termino){
	return this;
};

tipoDeFuncion.prototype.sust = function(nombreVariable,termino){
	return this;
};

variable.prototype.sust = function(nombreVariable,termino){
	if(this.nombre == nombreVariable){
		return termino.deepCopy();
	}else{
		return this;
	}
};

abstraccion.prototype.sust = function(nombreVariable,termino){
	if(this.variable.nombre != nombreVariable){
		this.codigo = this.codigo.sust(nombreVariable,termino);
	}
	return this;
};

aplicacion.prototype.sust = function(nombreVariable,termino){
	this.izquierdo = this.izquierdo.sust(nombreVariable,termino);
	this.derecho = this.derecho.sust(nombreVariable,termino);
	return this;
};

// TESTS -----------------------------------------------------------------------

// Tests de sustitución
if (TT.sust("x",FF) != TT){
	console.log("Falló el test de sustitución para TT.");
}

if (FF.sust("x",Bool) != FF){
	console.log("Falló el test de sustitución para FF.");
}

if (Bool.sust("x",FF) != Bool){
	console.log("Falló el test de sustitución para Bool.");
}

if (
	tipoDeFuncionBoolEnBool.sust("x",FF).tipoDominio != Bool ||
	tipoDeFuncionBoolEnBool.sust("x",FF).tipoImagen != Bool){
	console.log("Falló el test de sustitución para tipo de función Bool->Bool.");
}

if(
	new variable("x").sust("x",FF) != FF ||
	new variable("x").sust("y",FF).toString() != new variable("x").toString()){
	console.log("Falló el test de sustitución para variable.");
}

if (
	new abstraccion(new variable("y"),Bool,new variable("x")).sust("x",FF).toString() !=
	new abstraccion(new variable("y"),Bool,FF).toString()){
	console.log("Falló el test de sustitución para abstracción.");
}

if (
	new aplicacion(new abstraccion(new variable("x"),Bool,new variable("x")),
		new aplicacion(new abstraccion(new variable("y"),Bool,new variable("x")),TT)).sust("x",TT).toString() !=
	new aplicacion(new abstraccion(new variable("x"),Bool,new variable("x")),
		new aplicacion(new abstraccion(new variable("y"),Bool,TT),TT)).toString()){
	console.log("Falló el test de sustitución para aplicación.");
}

// EJERCICIO 5 -----------------------------------------------------------------
aplicacion.prototype.reducir = function(){
	if (!this.izquierdo.esFormaNormal) { // u
		this.izquierdo = this.izquierdo.reducir();
		return this;
	}else{
		if (!this.derecho.esFormaNormal) { // v
			this.derecho = this.derecho.reducir();
			return this;
		}else{ // beta
			return this.izquierdo.codigo.sust(this.izquierdo.variable,this.derecho);
		}
	}
};

// TESTS -----------------------------------------------------------------------

// Tests de reducir
if(trueAplicadoALaIdentidad.reducir().toString() != TT.toString()) {
	console.log("Falló el test de reducción para aplicación simple.");
}

if(
	new aplicacion(identidadDeBooleanos,trueAplicadoALaIdentidad).reducir().toString() != trueAplicadoALaIdentidad.toString()){
	console.log("Falló el test de reducción para aplicación compleja.");
}

// EJERCICIO 6 -----------------------------------------------------------------
//FUNCION EVALUAR IMPLEMENTADA EN EL PROTOTIPO expresion

// TESTS -----------------------------------------------------------------------

// Tiene que evaluarse y reducirse por completo, llegando al valor TT
if(trueAplicadoALaIdentidad.evaluar() != TT){
	console.log("Falló el test de evaluación para la abstracción simple.");
}

// Tiene que evaluarse y reducirse por completo, llegando al valor TT
if(
	new aplicacion(identidadDeBooleanos,trueAplicadoALaIdentidad).evaluar() != TT){
	console.log("Falló el test de evaluación para la abstracción compleja.");
}

// EJERCICIO 7 -----------------------------------------------------------------
Trait = {};

Trait.deepCopy2 = function(o){
	var res, v, key;
   res = {};
   for (key in o) {
		 v = o[key];
     res[key] = (typeof v !== "string" && !v.esValor) ? Trait.deepCopy2(v) : v;
   }
   return res;
};

deepCopyTrait = {deepCopyT : Trait.deepCopy2(this)};

Object.assign(expresion,deepCopyTrait);
Object.assign(tipo,deepCopyTrait);

// TESTS -----------------------------------------------------------------------

expresionOriginal = new abstraccion(new variable("x"),Bool,new variable("x"));
expresionCopiada = Trait.deepCopy2(expresionOriginal);
expresionCopiada.tipoDeVariable = new tipoDeFuncion(Bool,Bool);

// Copio una abstracción y le modifico el tipo de variable, comparo que la expresion original sigue igual
if(!(expresionOriginal.tipoDeVariable == Bool) || !((expresionCopiada.tipoDeVariable.toString()) == "Bool->Bool")){
	console.log("Falló el test de modificación de expresión copiada mediante deepCopyTrait.");
}
